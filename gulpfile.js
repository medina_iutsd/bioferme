"use strict";

const { src, dest, series, parallel } = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

function css() {
  return src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./dist/css'));
};

function html() {
  return src(['./src/index.html', './src/pages/**/*.html'])
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./dist'));
};

exports.default = parallel(html, css);
